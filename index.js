// MONGODB AGGREGATION


/*

- used to generate 

*/




db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
		]);




db.fruits.find();





db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
	]);




// Field Projection with aggregation


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$project: {_id: 0}}     // to hide supplier_id
	]);






// Sorting aggregated results


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$sort: {total: 1}},
	]);






// Aggregating results based on array fields



db.fruits.aggregate(
	[
		{$unwind: "$origin"},
		{$group: {_id: "$origin", fruts: {$sum: 1}}}
	]
)




db.fruits.find();






// OTHER Aggregate stages


db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$count: "Yellow Fruits"}

])





// $avg

// gets the average value of stock


db.fruits.aggregate(
	[
		{$match: {color: "Yellow"}},
		{$group: {_id: "$color", yellow_fruits_stock: {$avg: "$stock"}}}
	]

)







// $min - minimum
// $max - maximum

db.fruits.aggregate(
	[
		{$match: {color: "Yellow"}},
		{$group: {_id: "color", yellow_fruits_stock: {$min: "$stock"}}}
	]
)


db.fruits.aggregate(
	[
		{$match: {color: "Yellow"}},
		{$group: {_id: "color", yellow_fruits_stock: {$max: "$stock"}}}
	]
)







